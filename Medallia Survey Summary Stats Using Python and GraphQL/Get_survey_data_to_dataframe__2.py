# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 07:49:13 2022

@author: s267894

This script extracts survey columns of interest using Medallia's Query API
and converts the results into a pandas dataframe. It will also give summary
stats on survey volumes and NPS scores.
"""

# Import packages
import requests
import json
import pandas as pd
from collections import deque
import re

client_id = 'heb_query_api'
client_secret = ''

# Enter range for querying number of surveys
start_date = '"2022-04-13 00:00:00"'
end_date = '"2022-04-13 23:59:59"'

# Request authorization token
token_request = requests.post('https://heb.medallia.com/oauth/heb/token'
                         , data={'grant_type': 'client_credentials'}
                         , auth=(client_id, client_secret))

# Parse access token
access_token = json.loads(token_request.text)['access_token']

# List columns that will be applied in the resulting dataframe
mainColumns = ["survey_id",
"responsedate",
"survey_program",
"q_hebweb_ordering_experience",
"q_hebweb_ease_of_nav_5star",
"q_hebweb_timeslot_avail_5star",
"q_hebweb_frequency_mc",
"q_hebweb_how_cust_shop_mc",
"q_hebweb_who_cust_shops_brick_mc",
"q_hebweb_who_cust_shops_online_mc",
"q_hebweb_why_competitors_mc",
"q_hebweb_reason_for_shop_heb_mc_enum",
"q_hebweb_reason_for_shop_heb_mc",
"q_hebweb_likelihood_use_again_5star",
"q_hebweb_likelihood_to_use_again_5star",
"q_hebweb_product_variety_5star",
"q_hebweb_search_5star",
"q_hebweb_ease_of_checkout_5star",
"q_hebweb_order_rating_cmt",
"q_hebweb_more_time_yn",
"q_hebweb_frequency_mc_enum",
"q_heb_supplierofx_osat_5star",
"q_heb_supplierofx_osat_cmt",
"q_heb_supplierofx_email",
"q_heb_supplierportal_osat_5star",
"q_heb_supplierportal_osat_cmt",
"q_heb_supplierportal_email",
"q_heb_suppliersite_osat_5star",
"q_heb_suppliersite_osat_cmt",
"q_heb_suppliersite_email",
"q_md_text_custom_parameter_field_2041",
"store_id",
"order_id",
"q_md_text_custom_parameter_field_2038",
"e_heb_sub_program_enum",
"heb_delivery_nps",
"q_heb_contact_email_txt",
"q_heb_delivery_ltr_cmt",
"q_heb_delivery_osat_scale9",
"q_heb_delivery_driver_friendliness_scale9",
"q_heb_delivery_timeliness_scale9",
"q_heb_delivery_handling_groceries_scale9",
"q_heb_delivery_item_quality_scale9",
"q_heb_delivery_product_temp_scale9",
"q_heb_delivery_contactless_safety_scale9",
"q_heb_delivery_bag_sat_scale9",
"q_heb_delivery_recieved_ontime_yn",
"q_heb_delivery_likelihood_to_use_again_scale9",
"q_heb_delivery_improvement_cmt",
"heb_curbside_nps",
"q_heb_curbside_ltr_cmt",
"q_heb_curbside_osat_scale9",
"q_heb_curbside_wait_time_scale9",
"q_heb_curbside_partner_friendliness_scale9",
"q_heb_curbside_handling_groceries_scale9",
"q_heb_curbside_item_quality_scale9",
"q_heb_curbside_curbside_safety_scale9",
"q_heb_curbside_bag_sat_scale9",
"q_heb_curbside_recieved_ontime_yn",
"q_heb_curbside_likelihood_to_use_again_scale9",
"q_heb_curbside_improvement_cmt",
"cm_curbside_nps",
"q_cm_curbside_ltr_cmt",
"q_cm_curbside_ease_of_web_scale9",
"q_cm_curbside_finding_items_scale9",
"q_cm_curbside_timeslot_avail_scale9",
"q_cm_curbside_ease_of_checkout_scale9",
"q_cm_curbside_osat_scale9",
"q_cm_curbside_received_ontime_yn",
"q_cm_curbside_parking_avail_scale9",
"q_cm_curbside_wait_time_scale9",
"q_cm_curbside_partner_friendliness_scale9",
"q_cm_curbside_completeness_scale9",
"q_cm_curbside_item_quality_scale9",
"q_cm_curbside_likelihood_to_use_again_scale9",
"q_cm_curbside_improvement_cmt",
"cm_delivery_nps",
"q_cm_delivery_ltr_cmt",
"q_cm_delivery_ease_of_web_scale9",
"q_cm_delivery_finding_items_scale9",
"q_cm_delivery_timeslot_avail_scale9",
"q_cm_delivery_ease_of_checkout_scale9",
"q_feedback_chd_overall_how_satisfied_are_you",
"q_cm_delivery_received_ontime_yn",
"q_cm_delivery_timeliness_scale9",
"q_cm_delivery_driver_friendliness_scale9",
"q_cm_delivery_completeness_scale9",
"q_cm_delivery_item_quality_scale9",
"q_cm_delivery_likelihood_to_use_again_scale9",
"q_cm_delivery_improvement_cmt",
"jvss_curbside_nps",
"q_jvss_curbside_ltr_cmt",
"q_jvss_curbside_ease_of_app_scale9",
"q_jvss_curbside_finding_items_scale9",
"q_jvss_curbside_timeslot_avail_scale9",
"q_jvss_curbside_ease_of_checkout_scale9",
"q_jvss_curbside_osat_scale9",
"q_jvss_curbside_received_ontime_yn",
"q_jvss_curbside_parking_avail_scale9",
"q_jvss_curbside_wait_time_scale9",
"q_jvss_curbside_partner_friendliness_scale9",
"q_jvss_curbside_completeness_scale9",
"q_jvss_curbside_item_quality_scale9",
"q_jvss_curbside_likelihood_to_use_again_scale9",
"e_bp_digital_browser_name_auto",
"e_bp_digital_device_hardware_type_auto",
"k_bp_digital_url_auto",
"e_bp_digital_unit",
"k_bp_digital_url_path_auto",
"e_bp_digital_device_vendor_auto",
"e_bp_digital_device_model_auto",
"topic_tags"]

# Query Medallia data using Query API
query =  """query{
  feedback( 
    filter: {
      and: [
        { fieldIds: "e_responsedate", gte: """ + start_date + """}""" + """
         

        { fieldIds: "e_responsedate", lte: """ + end_date + """ }""" + """
      ]
    }
    first: 1000000
  ) {
    nodes {
      surveyId: id
 
      requested_field_list: fieldDataList(fieldIds: ["a_surveyid",
"e_responsedate",
"k_heb_global_feedback_program_enum",
"q_hebweb_ordering_experience",
"q_hebweb_ease_of_nav_5star",
"q_hebweb_timeslot_avail_5star",
"q_hebweb_frequency_mc",
"q_hebweb_how_cust_shop_mc",
"q_hebweb_who_cust_shops_brick_mc",
"q_hebweb_who_cust_shops_online_mc",
"q_hebweb_why_competitors_mc",
"q_hebweb_reason_for_shop_heb_mc_enum",
"q_hebweb_reason_for_shop_heb_mc",
"q_hebweb_likelihood_use_again_5star",
"q_hebweb_likelihood_to_use_again_5star", 
"q_hebweb_product_variety_5star",
"q_hebweb_search_5star",
"q_hebweb_ease_of_checkout_5star",
"q_hebweb_order_rating_cmt",
"q_hebweb_more_time_yn",
"q_hebweb_frequency_mc_enum",
"q_heb_supplierofx_osat_5star",
"q_heb_supplierofx_osat_cmt",
"q_heb_supplierofx_email",
"q_heb_supplierportal_osat_5star",
"q_heb_supplierportal_osat_cmt",
"q_heb_supplierportal_email",
"q_heb_suppliersite_osat_5star",
"q_heb_suppliersite_osat_cmt",
"q_heb_suppliersite_email",
"q_md_text_custom_parameter_field_2041",
"k_global_storeid_txt",
"k_global_orderid_txt",
"q_md_text_custom_parameter_field_2038",
"e_heb_sub_program_enum",
"q_heb_delivery_ltr_scale11",
"q_heb_contact_email_txt",
"q_heb_delivery_ltr_cmt",
"q_heb_delivery_osat_scale9",
"q_heb_delivery_driver_friendliness_scale9",
"q_heb_delivery_timeliness_scale9",
"q_heb_delivery_handling_groceries_scale9",
"q_heb_delivery_item_quality_scale9",
"q_heb_delivery_product_temp_scale9",
"q_heb_delivery_contactless_safety_scale9",
"q_heb_delivery_bag_sat_scale9",
"q_heb_delivery_recieved_ontime_yn",
"q_heb_delivery_likelihood_to_use_again_scale9",
"q_heb_delivery_improvement_cmt",
"q_heb_curbside_ltr_scale11",
"q_heb_curbside_ltr_cmt",
"q_heb_curbside_osat_scale9",
"q_heb_curbside_wait_time_scale9",
"q_heb_curbside_partner_friendliness_scale9",
"q_heb_curbside_handling_groceries_scale9",
"q_heb_curbside_item_quality_scale9",
"q_heb_curbside_curbside_safety_scale9",
"q_heb_curbside_bag_sat_scale9",
"q_heb_curbside_recieved_ontime_yn",
"q_heb_curbside_likelihood_to_use_again_scale9",
"q_heb_curbside_improvement_cmt",
"q_cm_curbside_ltr_scale11",
"q_cm_curbside_ltr_cmt",
"q_cm_curbside_ease_of_web_scale9",
"q_cm_curbside_finding_items_scale9",
"q_cm_curbside_timeslot_avail_scale9",
"q_cm_curbside_ease_of_checkout_scale9",
"q_cm_curbside_osat_scale9",
"q_cm_curbside_received_ontime_yn",
"q_cm_curbside_parking_avail_scale9",
"q_cm_curbside_wait_time_scale9",
"q_cm_curbside_partner_friendliness_scale9",
"q_cm_curbside_completeness_scale9",
"q_cm_curbside_item_quality_scale9",
"q_cm_curbside_likelihood_to_use_again_scale9",
"q_cm_curbside_improvement_cmt",
"q_cm_delivery_ltr_scale11",
"q_cm_delivery_ltr_cmt",
"q_cm_delivery_ease_of_web_scale9",
"q_cm_delivery_finding_items_scale9",
"q_cm_delivery_timeslot_avail_scale9",
"q_cm_delivery_ease_of_checkout_scale9",
"q_feedback_chd_overall_how_satisfied_are_you",
"q_cm_delivery_received_ontime_yn",
"q_cm_delivery_timeliness_scale9",
"q_cm_delivery_driver_friendliness_scale9",
"q_cm_delivery_completeness_scale9",
"q_cm_delivery_item_quality_scale9",
"q_cm_delivery_likelihood_to_use_again_scale9",
"q_cm_delivery_improvement_cmt",
"q_jvss_curbside_ltr_scale11",
"q_jvss_curbside_ltr_cmt",
"q_jvss_curbside_ease_of_app_scale9",
"q_jvss_curbside_finding_items_scale9",
"q_jvss_curbside_timeslot_avail_scale9",
"q_jvss_curbside_ease_of_checkout_scale9",
"q_jvss_curbside_osat_scale9",
"q_jvss_curbside_received_ontime_yn",
"q_jvss_curbside_parking_avail_scale9",
"q_jvss_curbside_wait_time_scale9",
"q_jvss_curbside_partner_friendliness_scale9",
"q_jvss_curbside_completeness_scale9",
"q_jvss_curbside_item_quality_scale9",
"q_jvss_curbside_likelihood_to_use_again_scale9",
"e_bp_digital_browser_name_auto",
"e_bp_digital_device_hardware_type_auto",
"k_bp_digital_url_auto",
"e_bp_digital_unit",
"k_bp_digital_url_path_auto",
"e_bp_digital_device_vendor_auto",
"e_bp_digital_device_model_auto",
"a_topics_sentiments_tagged_original"]) {
        values
      }
    }
    totalCount
  }
}"""


# Create headers dictionary with access token
headers = {
    'Authorization': 'Bearer ' + access_token,
    'Content-Type': 'application/json',
}

# Request survey count
query_request = requests.post('https://heb.apis.medallia.com/data/v0/query', headers=headers, json={"query": query})

# Load survey to json
survey_data = json.loads(query_request.text)

# Total surveys captured
survey_count = len(survey_data["data"]["feedback"]["nodes"])

# Create empty deque
deq = deque()

# Append data to deque
for i in range(survey_count):
    survey_record = survey_data["data"]["feedback"]["nodes"][i]["requested_field_list"]
    deq.append(survey_record)
    
# Change the deque to a dataframe
deq_to_df = pd.DataFrame(deq)

def extractFromDict(aDict):
    """Function which is applied to all columns in deq_to_df
       and extracts the actual data point"""
    aList = aDict["values"]
    if aList:
        return ",".join(aList)
    
# Apply extractFromDict to all columns 
survey_program_df = deq_to_df.applymap(lambda x: extractFromDict(x))

# Create column headers
survey_program_df.columns = mainColumns

def survey_desc(survey_id):  
    """If else statements to label survey description"""
    if survey_id == '1':
        return 'CM Talk To Us'
    elif survey_id == '2':
        return 'CM Supplier Contact Us'
    elif survey_id == '3':
        return 'HEB Curbside'
    elif survey_id == '4':
        return 'HEB Order Completion Curbside'
    elif survey_id == '5':
        return 'HEB Home Delivery'
    elif survey_id == '6':
        return 'CM Curbside'
    elif survey_id == '7':
        return 'CM Home Delivery'
    elif survey_id == '8':
        return 'JVSS Curbside'
    elif survey_id == '9':
        return 'HEB Supplier Feedback'
    elif survey_id == '10':
        return 'HEB Supplier Site'
    elif survey_id == '11':
        return 'HEB Supplier Portal'
    elif survey_id == '12':
        return 'HEB Web Supplier OFX'
    elif survey_id == '13':
        return 'HEB Order Completion Home Delivery'
    elif survey_id == '14':
        return 'JVSS Contact Us'
    elif survey_id == '15':
        return 'CM Private Event'
    else:
        return 'Missing Survey Id'
    
def changeToEmptyStr(aString):
    """Will use to map type None to empty string"""
    if aString is None:
        return ""
    else:
        return aString
    
def correctOrderNum(order_id):
    """Cleans unwanted text in order id column"""
    if order_id.find('HEB') != -1 or order_id.find('CMT') != -1 or order_id.find('JVS') != -1:
        splitTextTemp = str(order_id)
        splitText = splitTextTemp.split()
        for string in splitText:
            if string.find('HEB') != -1 or string.find('CMT') != -1 or string.find('JVS') != -1:
                eCommOrd = string
                break
        numbers = eCommOrd[3:]
        betterNumbers = re.sub("[^0-9]", "", numbers)
        if order_id.find('HEB') != -1:
            eCommOrdValueClean = 'HEB'+betterNumbers
        elif order_id.find('CMT') != -1:
            eCommOrdValueClean = 'CMT'+betterNumbers
        elif order_id.find('JVS') != -1:
            eCommOrdValueClean = 'JVS'+betterNumbers 
        return eCommOrdValueClean
    elif order_id.find('dup') != -1:
        return 'invalid order number (dup, test, etc.)'
    else:
        return order_id

# Apply survey id mapping
survey_program_df['survey_desc'] = survey_program_df['survey_program'].apply(lambda x: survey_desc(x))

# Correct the order number
survey_program_df['clean_order_id'] = survey_program_df['order_id'].apply(lambda x: correctOrderNum(str(x)))

# Filter out inaccurate web order id's
survey_program_df = survey_program_df[survey_program_df['clean_order_id'] != 'invalid order number (dup, test, etc.)']

survey_program_df = survey_program_df.applymap(lambda x: changeToEmptyStr(x))

# Set columnList to columns for overall nps score
columnList = ["heb_delivery_nps","heb_curbside_nps","cm_curbside_nps","cm_delivery_nps","jvss_curbside_nps"]
survey_program_df['post_order_nps_overall'] = survey_program_df[columnList].apply(lambda x: max(x), axis = 1)

# Set columnList to columns for overall heb score
columnList = ["heb_delivery_nps","heb_curbside_nps"]
survey_program_df['post_order_nps_heb'] = survey_program_df[columnList].apply(lambda x: max(x), axis = 1)

# Set columnList to columns for overall cm score
columnList = ["cm_curbside_nps","cm_delivery_nps"]
survey_program_df['post_order_nps_cm'] = survey_program_df[columnList].apply(lambda x: max(x), axis = 1)

# Print suumary
print("\n" + "Summary of surveys between " + start_date + " and " + end_date + "...\n")
print("Total HEB Curbside surveys is: " +str(survey_program_df[survey_program_df['survey_desc'] == 'HEB Curbside'].shape[0]) + "\n") 
print("Total HEB Home Delivery surveys is: " +str(survey_program_df[survey_program_df['survey_desc'] == 'HEB Home Delivery'].shape[0])+ "\n")
print("Total CM Curbside surveys is: " +str(survey_program_df[survey_program_df['survey_desc'] == 'CM Curbside'].shape[0])+ "\n")
print("Total CM Home Delivery surveys is: " +str(survey_program_df[survey_program_df['survey_desc'] == 'CM Home Delivery'].shape[0])+ "\n")
print("Total missing survey names is: " +str(survey_program_df[survey_program_df['survey_desc'] == 'Missing Survey Id'].shape[0])+ "\n")
print("Grand total surveys is: " +str(survey_count)+ "\n")

def calculate_nps(scores):
    """Takes in a series title (as a string) of nps answers and returns nps results"""
    nps_all_answer_types = ['0','1','2','3','4','5','6','7','8','9','10']
    nps_top_answer_types = ['9','10']
    nps_bottom_answer_types = ['0','1','2','3','4','5','6']
    total_count = survey_program_df[scores][survey_program_df[scores].apply(lambda x: x in nps_all_answer_types)].count()
    top_count = survey_program_df[scores][survey_program_df[scores].apply(lambda x: x in nps_top_answer_types)].count()
    bottom_count = survey_program_df[scores][survey_program_df[scores].apply(lambda x: x in nps_bottom_answer_types)].count()
    nps = (top_count/total_count) - (bottom_count/total_count)
    print("\n" + "NPS for " + scores +": " + str(round(nps,4)*100))
    
calculate_nps('post_order_nps_overall')  
calculate_nps('post_order_nps_heb') 
calculate_nps('heb_delivery_nps') 
calculate_nps('heb_curbside_nps') 
calculate_nps('post_order_nps_cm')   
calculate_nps('cm_curbside_nps')     
calculate_nps('cm_delivery_nps')
