This directory contains one python file which uses GraphQL to query Medallia's survey data. Medallia is
HEB's survey vendor for ecommerce. The script takes in a client id and client secret to determine 
survey counts and NPS (net promotor score) results for a given timeframe. The script was provided to 
Data Engineers at HEB to create an alert system which will notify Partners if the results of this script
do not match data being pushed from Medallia to HEB.

More information on Medallia and Medallia's query API can be found here:

https://hebecom.atlassian.net/wiki/spaces/DS/pages/2789933379/Requirements+to+Develop+Query+API+QA+Process