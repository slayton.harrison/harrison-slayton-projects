A Partner at HEB came to my team asking for help on automating some summary statistics
for campaign analysis. The Partner requested a script which would produce summary 
statistics based on a pre-defined MIC list and a pre-defined category list. The Partner
requested we use a presecribed template that can be edited and imported as needed.

The .xlsx files in this directory are the templates provided. The .py files were written
to iport the .xlsx files and query Teradata to provide resulting dataframes.