# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 09:45:31 2022

@author: s267894
"""


import pandas as pd

def mic_query(stringOfMics, connection):
    sql = """select	a14.ID_MIC  ID_MIC, max(a16.DSC_MIC)  DSC_MIC,
        	a13.ID_CONSM_UNT_CD  ID_CONSM_UNT_CD2,
        	max(a13.DSC_CONSM_UNT_CD)  DSC_CONSM_UNT_CD,
        	a13.ID_CAT  ID_CAT,
        	max(a13.DSC_CAT)  DSC_CAT,
        	a13.ID_BRAND  ID_BRAND,
        	max(a13.DSC_BRAND)  DSC_BRAND,
        	a13.ID_COMM  ID_COMM,
        	max(a13.DSC_COMM)  DSC_COMM,
        	a13.ID_SUB_COMM  ID_SUB_COMM,
        	max(a15.DSC_SUB_COMM)  DSC_SUB_COMM
        from	MSI_VW.V_DIM_PRODUCT	a13
        	join	MSI_VW.V_WDIM_REL_MIC_UPC	a14
        	  on 	(a13.ID_CONSM_UNT_CD = a14.ID_CONSM_UNT_CD)
        	join	MSI_VW.V_DIM_SUB_COMM	a15
        	  on 	(a13.ID_SUB_COMM = a15.ID_SUB_COMM)
        	join	MSI_VW.V_WDIM_MIC	a16
        	  on 	(a14.ID_MIC = a16.ID_MIC)
        where	a14.ID_MIC in ("""+stringOfMics+""")
        group by	a14.ID_MIC,
        	a13.ID_CONSM_UNT_CD,
        	a13.ID_CAT,
        	a13.ID_BRAND,
        	a13.ID_COMM,
        	a13.ID_SUB_COMM"""
    
    return pd.read_sql(sql,connection)

def eBskt_Pntn_mic(stringOfMics,stringOfAdWkId,connection):
    sql = """ 
        SELECT A.*
    , B.ORD_EXCLD_CNT
    , CAST(A.TXN_COUNT_ITEM_SEL_EX AS FLOAT) / CAST(B.ORD_EXCLD_CNT AS FLOAT)*100 AS BASKET_PNTN_PCNT_EX
    FROM (	
    	select	a17.ID_MIC,
    	a14.CHNL_KEY  CHNL_KEY,
    	a14.CHNL_DES  CHNL_DES,
    	a13.ID_CAT  ID_CAT,
    	a13.DSC_CAT DSC_CAT,
    	a12.ID_AD_WK,
    	a18.DSC_AD_WK  DSC_AD_WK,
    	a18.ID_AD_WK_BEG_DT,
    	a18.ID_AD_WK_END_DT,
    	sum(a11.AMT_NET_SLS_REV)  AS ACTUAL_SALES_ITEM, 
    	sum(a11.QTY_NET_PKG_ITMS)  AS SCAN_ITEMS_ITEM, 
    	count(distinct a11.ID_SHOPPING_TRANS)  AS TXN_COUNT_ITEM_SEL_EX 
    	from	MSI_VW.V_FACT_MKT_BSKT	a11
    		join	MSI_VW.VL_DIM_DATE	a12
    		  on 	(a11.ID_DATE = a12.ID_DATE)
    		join	MSI_VW.V_DIM_PRODUCT	a13
    		  on 	(a11.ID_CONSM_UNT_CD = a13.ID_CONSM_UNT_CD)
    		join	MSI_VW.V_FACT_ORD_TO_RCPT_BY_CUST	a14
    		  on 	(a11.ID_SHOPPING_TRANS = a14.ID_SHOPPING_TRANS)
    		join	MSI_VW.V_DIM_STORE	a15
    		  on 	(a11.KEY_STORE = a15.KEY_STORE)
    		join	MSI_VW.V_DIM_LOB	a16
    		  on 	(a15.ID_LOB = a16.ID_LOB)
    		join	MSI_VW.V_WDIM_REL_MIC_UPC	a17
    		  on 	(a13.ID_CONSM_UNT_CD = a17.ID_CONSM_UNT_CD)
    		join    MSI_VW.V_DIM_AD_WEEK a18
    		  on    (a12.ID_AD_WK = a18.ID_AD_WK)
    	where	(not (a13.ID_DEPT in (1, 0, 99, 8, 5))
    	 and a13.FLG_OWN_BRAND in ('N')
    	 and a14.CHNL_DES in ('Brick & Mortar', 'HEB2U')
    	 and a16.LIN_OF_BUS_GRP_ID in (10)
    	 and a17.ID_MIC in ("""+stringOfMics+""")
    	 and a12.ID_AD_WK in ("""+stringOfAdWkId+"""))
    	group by a17.ID_MIC,
    		a14.CHNL_KEY,
    	    a14.CHNL_DES,
    		a13.ID_CAT,
    		a13.DSC_CAT,
    		a12.ID_AD_WK,
    		a18.DSC_AD_WK,
    		a18.ID_AD_WK_BEG_DT,
    		a18.ID_AD_WK_END_DT
    ) A
    INNER JOIN (
    	select	a11.CHNL_KEY  CHNL_KEY,
    		a12.ID_AD_WK  ID_AD_WK,
    		sum(a11.ORD_EXCLD_CNT) ORD_EXCLD_CNT
    
    	from	MSI_VW.VL_FACT_SALS_PGM_CNT_DS	a11
    		join	MSI_VW.VL_DIM_DATE	a12
    		  on 	(a11.DT_ID = a12.ID_DATE)
    		join	MSI_VW.V_DIM_STORE	a13
    		  on 	(a11.STR_KEY = a13.KEY_STORE)
    		join	MSI_VW.V_DIM_LOB	a14
    		  on 	(a13.ID_LOB = a14.ID_LOB)
    	where	(a11.CHNL_DES in ('Brick & Mortar', 'HEB2U')
    	 and a14.LIN_OF_BUS_GRP_ID in (10)
    	 and (a12.ID_AD_WK in ("""+stringOfAdWkId+""")))
    	group by	a11.CHNL_KEY,
    		a12.ID_AD_WK
    ) B
    ON A.CHNL_KEY = B.CHNL_KEY
    AND A.ID_AD_WK = B.ID_AD_WK
        
    """
    
    return pd.read_sql(sql, connection)

def eBskt_Pntn_Category(stringOfCatId,stringOfAdWkId,connection):
    sql = """
    SELECT A.*
    , B.ORD_EXCLD_CNT
    , CAST(A.TXN_COUNT_ITEM_SEL_EX AS FLOAT) / CAST(B.ORD_EXCLD_CNT AS FLOAT)*100 AS BASKET_PNTN_PCNT_EX
    FROM (
    	select	a14.CHNL_KEY  CHNL_KEY,
    		a14.CHNL_DES  CHNL_DES,
    		a13.ID_CAT  ID_CAT,
    		a13.DSC_CAT DSC_CAT,
    		a12.ID_AD_WK  ID_AD_WK,
    		a18.DSC_AD_WK  DSC_AD_WK,
    		a18.ID_AD_WK_BEG_DT,
    		a18.ID_AD_WK_END_DT,
    		sum(a11.AMT_NET_SLS_REV)  AS ACTUAL_SALES_ITEM,
    		sum(a11.QTY_NET_PKG_ITMS)   AS SCAN_ITEMS_ITEM,
    		count(distinct a11.ID_SHOPPING_TRANS)  AS TXN_COUNT_ITEM_SEL_EX 
    	from	MSI_VW.V_FACT_MKT_BSKT	a11
    		join	MSI_VW.VL_DIM_DATE	a12
    		  on 	(a11.ID_DATE = a12.ID_DATE)
    		join	MSI_VW.V_DIM_PRODUCT	a13
    		  on 	(a11.ID_CONSM_UNT_CD = a13.ID_CONSM_UNT_CD)
    		join	MSI_VW.V_FACT_ORD_TO_RCPT_BY_CUST	a14
    		  on 	(a11.ID_SHOPPING_TRANS = a14.ID_SHOPPING_TRANS)
    		join	MSI_VW.V_DIM_STORE	a15
    		  on 	(a11.KEY_STORE = a15.KEY_STORE)
    		join	MSI_VW.V_DIM_LOB	a16
    		  on 	(a15.ID_LOB = a16.ID_LOB)
    		join    MSI_VW.V_DIM_AD_WEEK a18
    		  on    (a12.ID_AD_WK = a18.ID_AD_WK)
    	where	(not (a13.ID_DEPT in (1, 0, 99, 8, 5))
    	 and a13.FLG_OWN_BRAND in ('N')
    	 and a14.CHNL_DES in ('HEB2U', 'Brick & Mortar')
    	 and a16.LIN_OF_BUS_GRP_ID in (10)
    	 and a13.ID_CAT in ("""+stringOfCatId+""")
    	 and a12.ID_AD_WK in ("""+stringOfAdWkId+"""))
    	group by a14.CHNL_KEY,
    		    a14.CHNL_DES,
    			a13.ID_CAT,
    			a13.DSC_CAT,
    			a12.ID_AD_WK,
    			a18.DSC_AD_WK,
    			a18.ID_AD_WK_BEG_DT,
    			a18.ID_AD_WK_END_DT
    ) A
    INNER JOIN (
    	select	a11.CHNL_KEY  CHNL_KEY,
    		a12.ID_AD_WK  ID_AD_WK,
    		sum(a11.ORD_EXCLD_CNT)  ORD_EXCLD_CNT
    	from	MSI_VW.VL_FACT_SALS_PGM_CNT_DS	a11
    		join	MSI_VW.VL_DIM_DATE	a12
    		  on 	(a11.DT_ID = a12.ID_DATE)
    		join	MSI_VW.V_DIM_STORE	a13
    		  on 	(a11.STR_KEY = a13.KEY_STORE)
    		join	MSI_VW.V_DIM_LOB	a14
    		  on 	(a13.ID_LOB = a14.ID_LOB)
    	where	(a11.CHNL_DES in ('Brick & Mortar', 'HEB2U')
    	 and a14.LIN_OF_BUS_GRP_ID in (10)
    	 and (a12.ID_AD_WK in ("""+stringOfAdWkId+""")))
    	group by	a11.CHNL_KEY,
    		a12.ID_AD_WK
    ) B
    ON A.CHNL_KEY = B.CHNL_KEY
    AND A.ID_AD_WK = B.ID_AD_WK
    """
    return pd.read_sql(sql, connection)