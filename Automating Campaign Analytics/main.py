# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 09:13:46 2022

@author: s267894
"""

#Import pyodbc
import pyodbc
pyodbc.pooling = False
#Enter your onepass Id and  password
connection = pyodbc.connect('DSN=terada;UID=EnterOnePassId;PWD=EnterOnePassPassword')
connection.autocommit = True
import pandas as pd

#Import Sql Queries
from Sql_Queries import mic_query
from Sql_Queries import eBskt_Pntn_mic
from Sql_Queries import eBskt_Pntn_Category

#Path to import MIC list
micPath = 'eBasket_Pntn_MIC_Template.xlsx'
#Path to categories of interest
categoryPath = 'eBasket_Pntn_Category_Template.xlsx'


if __name__ == "__main__":
    #Import preselected MIC Ids
    micID = pd.read_excel(micPath)
    #Make a list of the imported MIC Ids
    micList = list(dict.fromkeys(list(micID.iloc[:,1])))
    #Changed the MIC list to a string seperated by commas
    micString = ",".join(map(str,micList))
    #Make timeframe list for mic template
    micTimeFrameList = list(dict.fromkeys(list(micID.iloc[:,3])))
    #Make the ad week list a string seperated by commas
    micAdWkString = ",".join(map(str,micTimeFrameList))


    #Import preselected categories
    catID = pd.read_excel(categoryPath)
    #Make a list of the imported category descriptions
    catList = list(dict.fromkeys(list(catID.iloc[:,1])))
    #Make the category list a string seperated by commas
    catString = ",".join(map(str,catList))
    #Make timeframe list for category template
    catTimeFrameList = list(dict.fromkeys(list(catID.iloc[:,3])))
    #Make the ad week list a string seperated by commas
    catAdWkString = ",".join(map(str,catTimeFrameList))
      
    #Query data for selected MICs from Teradata
    micData = mic_query(micString, connection )
    if not micData.empty:
        print("MIC Data Loaded!")
        
    #Query data for eBasket Penetration MIC report
    eBsktPenData = eBskt_Pntn_mic(micString, micAdWkString, connection)
    if not eBsktPenData.empty:
        print("eBasket Penetration MIC Data Loaded!")
        #Join to label the campaign type
        eBsktMicPenDataPrep = pd.merge(eBsktPenData
                                     , micID
                                     , how = 'inner'
                                     , left_on = ['ID_AD_WK', 'ID_MIC']
                                     , right_on = ['ID_AD_WK', 'MIC'])
        #Join to get the MIC description
        eBsktMicPenDataFinal = pd.merge(eBsktMicPenDataPrep
                        ,micData[['ID_MIC','DSC_MIC']].drop_duplicates()
                        ,how = 'inner'
                        ,left_on = 'ID_MIC'
                        ,right_on = 'ID_MIC')    
        #Sort data 
        eBsktMicPenDataFinal.sort_values(by = ['ID_MIC','ID_AD_WK','CHNL_DES']
                                      , axis = 0
                                      , ascending = True
                                      , inplace = True)
        #Choose final columns
        eBsktMicPenDataFinal = eBsktMicPenDataFinal[["CAMPAIGN_TYPE",
                                               "ID_MIC",
                                               "DSC_MIC",
                                               "ID_AD_WK",
                                               "DSC_AD_WK",
                                               "ID_AD_WK_BEG_DT",
                                               "ID_AD_WK_END_DT",
                                               "DSC_CAT",
                                               "CHNL_DES",
                                               "ACTUAL_SALES_ITEM",
                                               "SCAN_ITEMS_ITEM",
                                               "TXN_COUNT_ITEM_SEL_EX",
                                               "ORD_EXCLD_CNT",
                                               "BASKET_PNTN_PCNT_EX"]]
        
    eBsktPntCatData = eBskt_Pntn_Category(catString, catAdWkString, connection)
    if not eBsktPntCatData.empty:
        print("eBasket Penetration Category Data Loaded!")
        #Join to label the campaign type
        eBsktCatPenDataFinal = pd.merge(eBsktPntCatData
                                     , catID
                                     , how = 'inner'
                                     , left_on = ['ID_AD_WK', 'ID_CAT']
                                     , right_on = ['ID_AD_WK', 'Category_ID'])
        #Sort data 
        eBsktCatPenDataFinal.sort_values(by = ['ID_CAT','ID_AD_WK','CHNL_DES']
                                      , axis = 0
                                      , ascending = True
                                      , inplace = True)
        #Choose final columns
        eBsktCatPenDataFinal = eBsktCatPenDataFinal[["Campaign",
                                               "CAMPAIGN_TYPE",
                                               "ID_AD_WK",
                                               "DSC_AD_WK",
                                               "ID_AD_WK_BEG_DT",
                                               "ID_AD_WK_END_DT",
                                               "DSC_CAT",
                                               "CHNL_DES",
                                               "ACTUAL_SALES_ITEM",
                                               "SCAN_ITEMS_ITEM",
                                               "TXN_COUNT_ITEM_SEL_EX",
                                               "BASKET_PNTN_PCNT_EX"]]

        #Drop any dups
        eBsktCatPenDataFinal = eBsktCatPenDataFinal.drop_duplicates()
    #Close connection
    connection.close()

    
    
    